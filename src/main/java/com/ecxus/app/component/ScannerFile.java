package com.ecxus.app.component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.stereotype.Component;

@Component
public class ScannerFile {
    
    private final String N1 = "   |  |     ";
    private final String N2 = " _  _||_    ";
    private final String N3 = " _  _| _|   ";
    private final String N4 = "   |_|  |   ";
    private final String N5 = " _ |_  _|   ";
    private final String N6 = " _ |_ |_|   ";
    private final String N7 = " _   |  |   ";
    private final String N8 = " _ |_||_|   ";
    private final String N9 = " _ |_|  |   ";
    private final String N0 = " _ | ||_|   ";
    
    
    public String[] matrixToArrayNumber (String [][] pMatriz, int pPosLin, int pPosCol) {
        try
        {
            String [] array = new String [12];
            int pos = 0;
            int l = pPosLin;
            int c;
            for (int i = l; i <= (pPosLin + 3); i++) {
                c = pPosCol;
                for (int j = c; j<= (pPosCol + 2); j++) {
                    array[pos] = pMatriz[l][c];
                    c++;
                    pos++;
                }
                l++;                   
            }
            return array;
        }
        catch (Exception e) {
            throw e;
        }
    }
    
    public String pipeUnderlineToNumber(String[] s) {
        try
        {
            String strNumber = "";
            String str = printArrayToString(s);
            
            
            switch (str) {
                    case N0: strNumber += "0";
                             break;
                    case N1: strNumber += "1";
                             break;
                    case N2: strNumber += "2";
                             break;
                    case N3: strNumber += "3";
                             break;
                    case N4: strNumber += "4";
                             break;
                    case N5: strNumber += "5";
                             break;
                    case N6: strNumber += "6";
                             break;
                    case N7: strNumber += "7";
                             break;
                    case N8: strNumber += "8";
                             break;
                    case N9: strNumber += "9";
                             break;
                    default: strNumber += "?";
                            break;
                }
                               
            
            return strNumber;
        }
        catch (Exception e) {
            throw e;
        }
    }
    
    public int countLinesFile (String fileName) throws FileNotFoundException {
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            Long l = br.lines().count();
            br.lines().close();
            return Integer.parseInt(String.valueOf(l));
        }
        catch (FileNotFoundException | NumberFormatException e) {
            throw e;
        }
    }
    
    public boolean validatedNumber(char[] s) {
        try
        {
            int number = 0;
            int n = 0;
            while (n <= (s.length - 1)) {
                String st = String.valueOf(s[n]);
                switch (n) {
                    case 0: number += (Integer.parseInt(st) * 9);
                            break;
                    case 1: number += (Integer.parseInt(st) * 8);
                            break;
                    case 2: number += (Integer.parseInt(st) * 7);
                            break;
                    case 3: number += (Integer.parseInt(st) * 6);
                            break;
                    case 4: number += (Integer.parseInt(st) * 5);
                            break;
                    case 5: number += (Integer.parseInt(st) * 4);
                            break;
                    case 6: number += (Integer.parseInt(st) * 3);
                            break;
                    case 7: number += (Integer.parseInt(st) * 2);
                            break;
                    case 8: number += (Integer.parseInt(st) * 1);
                            break;
                    case 9: number += (Integer.parseInt(st));
                            break;
                    default: number += 0;
                            break;
                }
                n++;
            } 
            return (number % 11) == 0;
        }
        catch (NumberFormatException e) {
            throw e;
        }
    }
    
    
    
    public Collection <String> refactorNumber (char[] s) {
        try
        {
            String schar;
            Collection<String> lstFinal = new ArrayList<>();
            Collection<String> lstRef = new ArrayList<>();
            Collection<String> lstAdd = new ArrayList<>();
            lstRef.add(String.valueOf(s));
            lstFinal.add(String.valueOf(s));
            
            char [] local;
            int n = 0;
            
            while (n <= (s.length - 1)) {
                for (String strRef : lstRef) {
                    local = strRef.toCharArray();
                    schar = String.valueOf(local[n]);
                    switch (schar) {
                        case "1":
                            local[n] = "7".charAt(0);
                            break;
                        case "6":
                            local[n] = "8".charAt(0);
                            break;
                        case "7":
                            local[n] = "1".charAt(0);
                            break;
                        case "8":
                            local[n] = "0".charAt(0);
                            break;
                        case "0":
                            local[n] = "8".charAt(0);
                            break;
                        default:
                            break;
                    }
                    if (!lstFinal.contains(String.valueOf(local))) {
                        lstFinal.add(String.valueOf(local));
                        lstAdd.add(String.valueOf(local));
                    }
                    
                    
                }
                lstAdd.stream().filter((strAdd) -> (!lstRef.contains(strAdd))).forEachOrdered((strAdd) -> {
                    lstRef.add(strAdd);
                });
                lstAdd.clear();
                n++;
                
            }
            return lstFinal;
            
                    
        }catch (Exception e) {
            throw e;
        }
    }
    
    public String outputFileText(String[][] data) {
        String strNumber;
        String strError;
        int i;
        int c;
        final int col = 27;
        strNumber = "";
        Collection<String []> lstPipeUnderNoIdentify = new ArrayList<>();
        i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("Processo de escrita de arquivo.").append("\n");
        while (i <= (data.length - 1)) {
            c = 0;
            while (c < col) {
                String [] v = matrixToArrayNumber(data, i, c);
                strNumber += pipeUnderlineToNumber(v);
                if (pipeUnderlineToNumber(v).equals("?")) {
                    lstPipeUnderNoIdentify.add(v);
                }
                c = c + 3;
            }
            if (strNumber.contains("?")) {
                strError = "ILL";
                Collection<String> lstNewILL = refactorNumberILL(strNumber.toCharArray(), lstPipeUnderNoIdentify);
                sb.append(formatTextOutPut(strNumber, lstNewILL, strError).toString());
                
            } else if (validatedNumber(strNumber.toCharArray())) {
                sb.append(strNumber).append("\n");
            } else {
                strError = "ERR";
                Collection<String> lstNum = refactorNumber(strNumber.toCharArray());
                sb.append(formatTextOutPut(strNumber, lstNum, strError).toString());
            }
            
            
            strNumber = "";
            i = i + 4;
        }
        sb.append("Fim validacao arquivo.");
        return sb.toString();
    }
     
    public Collection<String> replacePipeUnderlineToString(String [] v) {
        try
        {
            Collection<String> lst = new ArrayList<>();
            int n = 0;
            String [] lv = v;
            if (lv.length > 0) {
                String str;
                while (n <= lv.length - 1)
                {
                    str = lv[n];
                    if (str.equals(" ")) {
                        lv[n] = lv[n].replace(" ","|");
                        
                        lst.add(printArrayToString(lv));
                        
                        lv[n] = lv[n].replace("|", "_");
                        
                        lst.add(printArrayToString(lv));
                        
                        lv[n] = lv[n].replace("_"," ");
                        
                    }
                    else if ((str.equals("|")) || (str.equals("_"))) {
                            lv[n] = " ";
                            lst.add(printArrayToString(lv));
                            if (str.equals("|")) {
                                lv[n] = "|";
                            } else lv[n] = "_";
                    }
                    n++;
                }
               
            }
            
            return lst;
           
            
        }catch (Exception e) {
            throw e;
        }
    }
    
    public Collection<String> refactorNumberILL (char [] s, Collection<String []> lstPipeUnderNoIdentify) {
        try
        {
            char [] local = s;
            Collection<String> lstNewText = new ArrayList<>();
            Collection<String> lstReplace = new ArrayList<>();
            int n = 0;
            String str;
            for (char ch: s) {
                str = String.valueOf(ch);
                if (str.equals("?")) {
                   String [] rm = null;
                   for (String [] vStr : lstPipeUnderNoIdentify) {
                       rm = vStr;
                       lstReplace = replacePipeUnderlineToString(vStr);
                       break;
                   }
                   lstPipeUnderNoIdentify.remove(rm);
                   if (!lstReplace.isEmpty()) {
                       for (String strReplace: lstReplace) {
                           String newCh = pipeUnderlineToNumber(strReplace.split(""));
                           if (!newCh.equals("?")) {
                                local[n] = newCh.charAt(0);
                                lstNewText.add(String.valueOf(local));
                           }
                       }
                   }
                }
                n++;
            }
            return lstNewText;
        }
        catch (Exception e) {
            throw e;
        }
    }
    
    public StringBuilder formatTextOutPut (String strNumber, Collection<String> lst, String strError) {
        try
        {
            StringBuilder sb = new StringBuilder();
            String strPrintNumber = "";
            String strPrintNumberZero = "";
            int cStr = 0;
            if (!lst.isEmpty()) {

                for (String strNum : lst) {
                    if (validatedNumber(strNum.toCharArray())) {
                        switch (cStr) {
                            case 0:
                                strPrintNumberZero = strNum;
                                break;
                            case 1:
                                strPrintNumber += strNum;
                                break;
                            default:
                                strPrintNumber +=" , " + strNum ;
                                break;   
                        }
                        cStr++;                            
                    }
                }

            }
            if (cStr > 1) {
                sb.append(strNumber).append(" AMB [").append(strPrintNumberZero).append(",").append(strPrintNumber).append("]\n");
            }
            else if (cStr == 1) {
                sb.append(strPrintNumberZero).append("\n");
            }
            else sb.append(strNumber).append(" ").append(strError).append("\n");
            return sb;
        }catch (Exception e) {
            throw e;
        }
    }
    
    
    private String printArrayToString(String [] v) {
        try
        {
            String str = "";
            for (String s: v) {
                str += s;
            }
            return str;
        }
        catch (Exception e) {
            throw e;
        }
    }
    
    
}
