package com.ecxus.app.config;

import com.ecxus.app.component.ScannerFile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "file")
@ComponentScan("com.ecxus.app.component")

public class FileManagement {
    
    @Autowired
    private ScannerFile scannerFile;
    
    private final static Logger LOGGER = Logger.getLogger(FileManagement.class.getName());
    
    private String output;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
    
    public String[][] readFile(String fileName) throws IOException {
        int l;
        int c;
            
        try
        {
            LOGGER.log(Level.INFO,"Leitura de arquivo input.txt...");
            int linhas = scannerFile.countLinesFile(fileName);
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            String [][] mat = new String[linhas][27];
            l = 0;
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                char[] array = line.toCharArray();
                c = 0;
                for (char ch: array){
                    String s = Character.toString(ch);
                    mat [l][c] = s;
                    c++;
                }
                l++;     
                line = br.readLine();
            }
            String everything = sb.toString();
            LOGGER.log(Level.INFO,everything);
            LOGGER.log(Level.INFO,"Arquivo input.txt leitura completa.");
            return mat;
        }
        catch (IOException e) {
            LOGGER.log(Level.SEVERE,"Erro ao ler aquivo input.text. Detalhes ..{0}",e.getMessage());
            throw e;
         
        }
    }  
    
    public void outputFile(String[][] data) throws IOException {
        try
        {
            LOGGER.log(Level.INFO,"Gerando arquivo output.txt...");
            Path path = Paths.get(this.getOutput());
            try (BufferedWriter writer = Files.newBufferedWriter(path))
            {
                String arqText = scannerFile.outputFileText(data);
                writer.write(arqText);
                LOGGER.log(Level.INFO,arqText);
                LOGGER.log(Level.INFO,"Arquivo output.txt gerado com sucesso.");
            }
        }
        catch (IOException e) {
            LOGGER.log(Level.SEVERE,"Erro ao gerar arquivo output.txt. Detlahes .. {0}",e.getMessage());
            throw e;
        }
    }
    

    
}
