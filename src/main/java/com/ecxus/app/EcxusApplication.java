package com.ecxus.app;

import com.ecxus.app.config.FileManagement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("file")
public class EcxusApplication implements CommandLineRunner {

    public static void main (String[] args) {
        SpringApplication.run(EcxusApplication.class, args);
    }

    
    @Autowired
    private FileManagement fileManagement;
    
    private String inputFileName;

    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    
    @Override
    public void run(String... args) throws Exception {
        try
        {
            Path pathResources = Paths.get(getClass().getClassLoader()
                    .getResource(this.getInputFileName()).toURI());
            String data[][] = fileManagement.readFile(pathResources.toString());
            
            fileManagement.outputFile(data);
                        
        }catch (IOException | URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
}

